﻿Public Class Form5
    Private Sub Form5_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        BACKGROUND.Close()
    End Sub

    Public Sub Form5_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.TopMost = True 'BIG FREAKING BLOCK, START INSTALLING
        Me.Update()
        Label1.Text = "Grabbing Information"
        Label1.Text = "Copying Files"
        If My.Settings.register = False Then
            files()
        End If
        If My.Settings.patch = True Then
            patchRun()
        End If
        ProgressBar1.Value = 50
        Me.Update()


        Label1.Text = "Registering Product"
        register()
        Label1.Text = "Cleaning Up Temporary Files"
        Label1.Text = "Done!"
        ProgressBar1.Value = 100 'Progress bars are useless here
        Me.Update()
        MessageBox.Show("Enjoy SC2k!")

    End Sub
    Public Sub files()
        If My.Settings.register = False Then
            Try
                My.Computer.FileSystem.CopyDirectory(My.Settings.location, My.Settings.store)
            Catch ex As Exception

                MessageBox.Show("The files seem to already be copied, I'll skip this part.") 'lazy programmer protection (for debugging mostly)
            End Try
        End If
    End Sub
    Public Sub patchRun()
        System.IO.File.SetAttributes(My.Settings.store & "\SIMCITY.EXE", IO.FileAttributes.Normal) 'Windows 10 sucks, if you don't set this, you get an access denied error. Friggin really?
        My.Computer.FileSystem.DeleteFile(My.Settings.store & "\SIMCITY.EXE")
        My.Computer.FileSystem.CopyFile("SIMCITY_PATCHED.EXE", My.Settings.store & "\SIMCITY.EXE") 'I hope we never include this on a CD or I'll have to change this to save the patched exe in a temp dir
        My.Computer.FileSystem.DeleteFile("SIMCITY_PATCHED.EXE")

    End Sub
    Public Sub register()
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\REGISTRATION", "Mayor Name", My.Settings.user) 'Me!
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\REGISTRATION", "Company Name", My.Settings.company) 'Mine!
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Localize", "Language", My.Settings.language)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Options", "Speed", "00000001", Microsoft.Win32.RegistryValueKind.DWord)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Options", "Sound", "00000001", Microsoft.Win32.RegistryValueKind.DWord)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Options", "Music", "00000001", Microsoft.Win32.RegistryValueKind.DWord)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Options", "AutoGoto", "00000001", Microsoft.Win32.RegistryValueKind.DWord)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Options", "AutoBudget", "00000000", Microsoft.Win32.RegistryValueKind.DWord)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Options", "Disasters", "00000000", Microsoft.Win32.RegistryValueKind.DWord)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Options", "AutoSave", "00000001", Microsoft.Win32.RegistryValueKind.DWord)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Paths", "Home", My.Settings.store)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Paths", "Graphics", My.Settings.store & "\Bitmaps")
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Paths", "Music", My.Settings.store & "\Sounds")
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Paths", "Data", My.Settings.store & "\Data")
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Paths", "Goodies", My.Settings.store & "\Goodies")
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Paths", "SaveGame", My.Settings.store & "\Cities")
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Paths", "Scenarios", My.Settings.store & "\Scenario")
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Version", "SimCity 2000", "00000100", Microsoft.Win32.RegistryValueKind.DWord)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Windows", "Display", "8 1")
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Windows", "Color Check", "00000000", Microsoft.Win32.RegistryValueKind.DWord)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Windows", "Last Color Depth", "00000020", Microsoft.Win32.RegistryValueKind.DWord)
        If My.Settings.menu = True Then 'I fixed these shortcuts by not being an idiot and actually reading the function inputs
            My.Computer.FileSystem.CreateDirectory(My.Computer.FileSystem.SpecialDirectories.Programs & "\Maxis\SimCity 2000")
            CreateShortCut(My.Settings.store & "\SIMCITY.EXE", My.Computer.FileSystem.SpecialDirectories.Programs & "\Maxis\SimCity 2000\", "SimCity 2000", My.Settings.store, 1, 0)

        End If
        If My.Settings.shortcut = True Then
            CreateShortCut(My.Settings.store & "\SIMCITY.EXE", My.Computer.FileSystem.SpecialDirectories.Desktop, "SimCity 2000", My.Settings.store, 1, 0)
        End If
        If My.Settings.SCURK = True Then
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\Paths", "TileSets", My.Settings.store & "\ScurkArt")
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\SCURK", "CycleColors", "00000001", Microsoft.Win32.RegistryValueKind.DWord)
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\SCURK", "GridHeight", "00000002", Microsoft.Win32.RegistryValueKind.DWord)
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\SCURK", "GridWidth", "2", Microsoft.Win32.RegistryValueKind.DWord)
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\SCURK", "ShowClipRegion", 0, Microsoft.Win32.RegistryValueKind.DWord)
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\SCURK", "ShowDrawGrid", 0, Microsoft.Win32.RegistryValueKind.DWord)
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\SCURK", "SnapToGrid", 0, Microsoft.Win32.RegistryValueKind.DWord)
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Maxis\SimCity 2000\SCURK", "Sound", 1, Microsoft.Win32.RegistryValueKind.DWord)
            If My.Settings.menu = True Then
                CreateShortCut(My.Settings.store & "\WINSCURK.EXE", My.Computer.FileSystem.SpecialDirectories.Programs & "\Maxis\SimCity 2000\", "SimCity Urban Renewal Kit", My.Settings.store, 1, 0) 'It's not SCURK.exe, it's WINSCURK.exe
            End If

        End If
        If My.Settings.associate = True Then
            'City
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\.sc2\", "", "SimCity2000.Document.City")
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\.sc2\", "Content Type", "SimCity2000.Document.City")
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.City\", "", "SimCity2000.Document.City")
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.City\", "FriendlyTypeName", "SimCity 2000 City")
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.City\DefaultIcon", "", My.Settings.store & "\SIMCITY.EXE,1")
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.City\shell", "", "open")
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.City\shell\open\command", "", My.Settings.store & "\SIMCITY.EXE %1")
            'Scenario
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\.scn\", "", "SimCity2000.Document.Scenario")
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\.scn\", "Content Type", "SimCity2000.Document.Scenario")
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.Scenario\", "", "SimCity2000.Document.Scenario")
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.Scenario\", "FriendlyTypeName", "SimCity 2000 Scenario")
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.Scenario\DefaultIcon", "", My.Settings.store & "\SIMCITY.EXE,2")
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.Scenario\shell", "", "open")
            My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.Scenario\shell\open\command", "", My.Settings.store & "\SIMCITY.EXE %1")


            If My.Settings.SCURK = True Then
                My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\.mif\", "", "SimCity2000.Document.TileSet")
                My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\.mif\", "Content Type", "SimCity2000.Document.TileSet")
                My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.TileSet\", "", "SimCity2000.Document.TileSet")
                My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.TileSet\", "FriendlyTypeName", "SimCity 2000 Tile Set")
                My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.TileSet\DefaultIcon", "", My.Settings.store & "\WINSCURK.EXE,1")
                My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.TileSet\shell", "", "open")
                My.Computer.Registry.SetValue("HKEY_CLASSES_ROOT\SimCity2000.Document.TileSet\shell\open\command", "", My.Settings.store & "\WINSCURK.EXE %1")
            End If
            'This reloads file icons
            Process.Start("taskkill.exe", "/IM " & Chr(34) & "explorer.exe" & Chr(34) & " /F")
            Threading.Thread.Sleep(500)
            System.Diagnostics.Process.Start("explorer.exe")
        End If
        ' Windows sucks
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        BACKGROUND.Close()
    End Sub
End Class
