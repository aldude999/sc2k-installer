﻿Imports System.Resources
Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SimCity 2000 Installer")>
<Assembly: AssemblyDescription("This application allows you to install SimCity 2000 on Windows 7 and above")>
<Assembly: AssemblyCompany("aldude999")>
<Assembly: AssemblyProduct("SimCity 2000 Installer")>
<Assembly: AssemblyCopyright("Copyright © aldude999 2012-2020")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("bdf145fa-76ce-4fc4-8a49-8da20f0a7c8f")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.0.0.3")>
<Assembly: AssemblyFileVersion("2.0.0.3")>
<Assembly: NeutralResourcesLanguage("en-US")>
