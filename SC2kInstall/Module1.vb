﻿Imports System.Reflection
Imports System.IO
Module module1

    Private Declare Function mciSendString Lib "winmm.dll" Alias "mciSendStringA" (ByVal lpstrCommand As String, ByVal lpstrReturnString As String, ByVal uReturnLength As Long, ByVal hwndCallback As Long) As Long
    Public Function PlayMidiFile(ByVal MidiFile As String) As Boolean
        'this plays the music!
        Dim lRet As Long
        On Error Resume Next
        If Dir(MidiFile) = "" Then Exit Function
        lRet = mciSendString("stop midi", "", 0, 0)
        lRet = mciSendString("close midi", "", 0, 0)
        lRet = mciSendString("open sequencer!" & MidiFile & " alias midi", "", 0, 0)
        lRet = mciSendString("play midi", "", 0, 0)
        PlayMidiFile = (lRet = 0)
    End Function
    Public Function StopMidiFile() As Boolean
        Dim lRet As Long
        On Error Resume Next
        lRet = mciSendString("stop midi", "", 0, 0)
        lRet = mciSendString("close midi", "", 0, 0)
        StopMidiFile = (lRet = 0)
    End Function

    Public Function CreateShortCut(ByVal TargetPath As String, ByVal ShortCutPath As String, ByVal ShortCutname As String, ByVal WorkPath As String, ByVal Window_Style As Integer, ByVal IconNum As Integer)
        'Shortcut function pulled off the internet somewhere
        Dim VbsObj As Object
        VbsObj = CreateObject("WScript.Shell")

        Dim MyShortcut As Object
        'ShortCutPath = VbsObj.SpecialFolders(ShortCutPath)
        MyShortcut = VbsObj.CreateShortcut(ShortCutPath & "\" & ShortCutname & ".lnk")
        MyShortcut.TargetPath = TargetPath
        MyShortcut.WorkingDirectory = WorkPath
        MyShortcut.WindowStyle = Window_Style
        MyShortcut.IconLocation = TargetPath & "," & IconNum
        MyShortcut.Save()


    End Function

    Public Function SaveToDisk(ByVal resourceName As String, ByVal fileName As String)
        ' Get a reference to the running application.
        Dim assy As [Assembly] = [Assembly].GetExecutingAssembly()

        ' Loop through each resource, looking for the image name (case-insensitive).
        For Each resource As String In assy.GetManifestResourceNames()
            If resource.ToLower().IndexOf(resourceName.ToLower) <> -1 Then
                ' Get the embedded file from the assembly as a MemoryStream.
                Using resourceStream As System.IO.Stream = assy.GetManifestResourceStream(resource)
                    If resourceStream IsNot Nothing Then
                        Using reader As New BinaryReader(resourceStream)
                            ' Read the bytes from the input stream.
                            Dim buffer() As Byte = reader.ReadBytes(CInt(resourceStream.Length))
                            Using outputStream As New FileStream(fileName, FileMode.Create)
                                Using writer As New BinaryWriter(outputStream)
                                    ' Write the bytes to the output stream.
                                    writer.Write(buffer)
                                End Using
                            End Using
                        End Using
                    End If
                End Using
                Exit For
            End If
        Next resource
    End Function

End Module
